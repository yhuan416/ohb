# ohb

#### 介绍

ohb是OpenHarmony Build的缩写，或者理解为Optimized HarmonyOS Build，优化后的hb

#### 我们要做什么

我们（一群无法容忍 hb 工具的复杂和混乱的人）正在尝试为 OpenHarmony 设计更好的构建解决方案。 
我们有以下目标：


- 尝试统一 L0、L1 和 L2 构建系统。
- 使构建系统更好地被理解。
- 尽量降低引入新板子的难度。
- 减少构建系统中使用的脚本或工具，少即是多。
- 如果可能，帮助减少 Open Harmony 的跨层依赖。

欢迎您的建议和参与。- 


