# ohb

#### Description
ohb stands for OpenHarmony Build, or Optimized HarmonyOS Build.

#### What we want to do?
We (a group of people who can not tolerate the complexity and chaos of hb tool) are trying to design a better build solution for OpenHarmony.
We have following goals:
1.  Try to unify L0, L1 and L2 build system.
2.  Make the build system better to be understood.
3.  Try to reduce the difficulty of introducing a new board.
4.  Reduce the scripts or tools used in the build system, less is more.
5.  If possile, help to reduce the cross-layer dependencies of Open Harmony.

Your suggestions and participantion are welcome.

Mail: 962030@qq.com/leo@hiharmonica.com

